# import the necessary packages
from skimage.measure import compare_ssim
import argparse
import imutils  # pipe install imutils
import cv2
import os # to manipulate files
import numpy as np
import statistics as st
from scipy.stats.mstats import mquantiles

# allow easy plotting
from matplotlib import pyplot as plt
def plot(img):
    plt.imshow(img);plt.show()
def hist(img):
    plt.hist(img.ravel(),256);plt.show()


args = {"first":"/home/khalid/Bureau/startLearn/EK000534_3.JPG",
        "second":"/home/khalid/Bureau/startLearn/EK000535_3.JPG",
        "mask":"/home/khalid/Bureau/startLearn/EK000351_mask.png"}


# load the two input images

imageA = cv2.imread(args["first"])
imageB = cv2.imread(args["second"])

landscapeMask = cv2.cvtColor(cv2.imread(args["mask"]),cv2.COLOR_BGR2GRAY) #Conversion entre deux espaces de couleur ( BGR >> Gray)

imageA = cv2.bitwise_and(imageA,imageA,mask=landscapeMask)  # compare un à un les bits de A et B. Si deux bits sont égaux à 1, 1 est placé à la même position dans le 								      résultat. Si l'un et/ou l'autre des bits contient 0, 0 est alors placé dans le résultat.
imageB = cv2.bitwise_and(imageB,imageB,mask=landscapeMask)  # mask = indique les elements du tableau en sortie à modifier


blurFact=25  # vas servir de largeur et hauteur pour le noyau (impairs et >0)
kernel = np.ones((blurFact,blurFact),np.uint8)  # Initialisation du noyau, il est du type numpy.ndarray de dim 25*25


b=cv2.GaussianBlur(imageB,(blurFact,blurFact),sigmaX=0)  # utilisation d'un filtre gaussien 
a=cv2.GaussianBlur(imageA,(blurFact,blurFact),sigmaX=0) # sigmaX l'écart type dans la direction X; sigmaX==sigmaY

(score, diff) = compare_ssim(a, b, full=True,multichannel=True) ## Calcule l'indice de similarité structurelle moyen entre deux images 
								## score :La similarité structurelle moyenne sur l'image
								## diff : renvoie l'image complète (un array remplie de 1)
diff = (diff * 255).astype("uint8")  ## multiplier diff par 255 la valeurs max
blur = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY) ## changer d'espace de couleur 



nameSecond = args["second"]
subImagesDir = nameSecond+"_subImages/"
if not os.path.exists(subImagesDir):
        os.makedirs(subImagesDir)


thresh = cv2.threshold(src=blur, thresh=mquantiles(diff.ravel(),prob=[0.001]), ## Binarisation de la photo
                       maxval=255,type=cv2.THRESH_BINARY_INV)[1] 
thresh = cv2.dilate(thresh,kernel) ## dilatation
thresh = cv2.erode(thresh,kernel,iterations=1) ## erosion

cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, ## détecte les contours 
                            cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts) ## fonction facultative findcontour peut retourner les contours

# changing parts
chParts = cv2.bitwise_and(imageB,imageB,mask=thresh) ## retourne l'intersection de l'imageB et le mask 
maskContours = np.zeros(imageB.shape, np.uint8)   ## matrice remplie de 0 donc image noire 


# loop over the contours and save sub-images
iSave = 1
imageRectanglesA = imageA.copy()
imageRectanglesB = imageB.copy()
imageContours = imageB.copy()
for ic in range(1,len(cnts)):

    (x, y, w, h) = cv2.boundingRect(cnts[ic])
    if w>blurFact/2 or h>blurFact/2: # il faudrait ajouter la distance et pondérer ça
        # on pourrait aussi éliminer d'entrée les trucs verts (feuilles qui bougent)
        # mais il faut aussi faire une interface pour entrer les masques
        # et les limites de parcelles
        cv2.rectangle(imageRectanglesA, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.rectangle(imageRectanglesB, (x, y), (x + w, y + h), (0, 0, 255), 2)
        cv2.drawContours(imageContours,[cnts[ic]],0, (0,255,0), 3)
        cv2.drawContours(maskContours,[cnts[ic]],0, (255,255,255), cv2.FILLED)

        # export sub-images
        buf = 50
        maxX = min(x+w+buf,imageB.shape[1]) # opencv use y first, x then
        minX = max(x-buf,0) # opencv use y first, x then
        maxY = min(y+h+buf,imageB.shape[0])
        minY = max(y-buf,0)
        maskContoursLoc = cv2.dilate(maskContours,kernel)
        changeOnly = cv2.bitwise_and(imageB,maskContoursLoc)      
        subI = imageB[minY:maxY,minX:maxX]
        cv2.imwrite(subImagesDir+"diffs"+str(iSave)+".png",subI)
        iSave = iSave+1

cv2.imwrite("diffsA.png",imageRectanglesA)
cv2.imwrite("diffsB.png",imageRectanglesB)
cv2.imwrite("blur.png",blur)
cv2.imwrite("drawContours.png",imageContours)



