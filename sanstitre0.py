#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 11:32:30 2019

@author: khalid
"""

# import the necessary packages
from skimage.measure import compare_ssim
import argparse
import imutils    # pip sudo install imutils
import cv2
import os # to manipulate files
import numpy as np
import statistics as st
from scipy.stats.mstats import mquantiles


